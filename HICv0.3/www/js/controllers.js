angular.module('app.controllers', ['satellizer', 'ui.router'])

.controller('startCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate, $auth) {
  setTimeout(function () {
    //$location.path();
    $location.path("/page6");
  }, 1000);
})
.controller('AuthCtrl', function($auth, $state,$http,$rootScope, $scope, $location, $timeout, $ionicPopup, $window) {
  $scope.doRefreshProfile=function(){
    $http({
      method: 'GET',
      url: 'http://testhic-foi15a1.rhcloud.com/api/countInvges'
    }).then ( function successCallback (response){
      $scope.countInvges = response.data
      console.log(response);
    },function errorCallback(response){
      console.log(response);
    });
    $http({
      method: 'GET',
      url: 'http://testhic-foi15a1.rhcloud.com/api/countShplistges'
    }).then ( function successCallback (response){
      $scope.countShplistges = response.data
      console.log(response);
    },function errorCallback(response){
      console.log(response);
    });
    $scope.$broadcast('scroll.refreshComplete');
    $http({
      method: 'GET',
      url: 'http://testhic-foi15a1.rhcloud.com/api/countInveinz'
    }).then ( function successCallback (response){
      $scope.countInveinz = response.data
      console.log(response);
    },function errorCallback(response){
      console.log(response);
    });
    $http({
      method: 'GET',
      url: 'http://testhic-foi15a1.rhcloud.com/api/countShplisteinz'
    }).then ( function successCallback (response){
      $scope.countShplisteinz = response.data
      console.log(response);
    },function errorCallback(response){
      console.log(response);
    });
  };


  $scope.email='';
  $scope.password='';
  $scope.newUser={};
  $scope.loginError=false;
  $scope.loginErrorText='';

  $scope.checkmail=function(email){
    $http({
      method: 'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/checkmail',
      data: {'email':email}
    }).then(function(response){
      $scope.checkmail_variable = response.data;
      if($scope.checkmail_variable==0){

        $scope.register();
      }
      else{

        $ionicPopup.alert({
          title: 'Falsche Eingabe',
          template: 'E-Mail Adresse bereits vorhanden!'
        });

      }

    });

  };

  $scope.login = function() {

    var credentials = {
      email: $scope.email,
      password: $scope.password
    }


    $auth.login(credentials).then(function() {

      return $http({
        method: 'get',
        url: 'http://testhic-foi15a1.rhcloud.com/api/authenticate_user'
      })
      //$httpget('http://testhic-foi15a1.rhcloud.com/api/authenticate/user');

    }, function(error) {
      $scope.loginError = true;
      $scope.loginErrorText = error.data.error;
      if ($scope.loginErrorText === 'invalid_credentials'){
        $scope.showAlertWrongCredetials();
      }
      else{
        $scope.showAlertFehler();
      }

    }).then(function(response) {
      $rootScope.currentUser = response.data.user;
      $scope.loginError = false;
      $scope.loginErrorText = '';



      $state.go('tabsController.inventory', {});
    });
    $auth.setStorageType('localStorage');
  };

  $scope.register = function () {

    $http.post('http://testhic-foi15a1.rhcloud.com/api/register',$scope.newUser)
    .success(function(data){
      $scope.email=$scope.newUser.email;
      $scope.password=$scope.newUser.password;
      $scope.login();
      $scope.newUser={};
    })
  };

  $scope.deleteCurentUser = function(){
    $rootScope.currentUser = null;
  }

  $scope.directToLogin = function(){
    $location.path();
    $location.path("/page6")
  }

  $scope.logout = function() {
    $auth.logout().then(function() {
      $scope.deleteCurentUser()
      $scope.directToLogin()
    });

  }
  $http({
    method: 'GET',
    url: 'http://testhic-foi15a1.rhcloud.com/api/getUserData'
  }).then ( function successCallback (response){
    $scope.UserData = response.data
    console.log(response);
  },function errorCallback(response){
    console.log(response);
  });

  $scope.deleteUser=function(){
    $http({
      method: 'GET',
      url: 'http://testhic-foi15a1.rhcloud.com/api/deleteUser',
    }).then(function successCallback(response){
      $ionicPopup.alert({
        title: 'Account gelöscht',
        template: 'Account wurde erforlgreich gelöscht'
      });
      $location.path();
      $location.path("/page6")
      console.log(response);
    }, function errorCallback(response){
      $ionicPopup.alert({
        title: 'Fehler',
        template: 'Ein Fehler ist aufgetreten!'
      });
      console.log(response);
    });

  }

  $http({
    method: 'GET',
    url: 'http://testhic-foi15a1.rhcloud.com/api/countInvges'
  }).then ( function successCallback (response){
    $scope.countInvges = response.data
    console.log(response);
  },function errorCallback(response){
    console.log(response);
  });



  $http({
    method: 'GET',
    url: 'http://testhic-foi15a1.rhcloud.com/api/countShplistges'
  }).then ( function successCallback (response){
    $scope.countShplistges = response.data
    console.log(response);
  },function errorCallback(response){
    console.log(response);
  });

  $http({
    method: 'GET',
    url: 'http://testhic-foi15a1.rhcloud.com/api/countInveinz'
  }).then ( function successCallback (response){
    $scope.countInveinz = response.data
    console.log(response);
  },function errorCallback(response){
    console.log(response);
  });
  $http({
    method: 'GET',
    url: 'http://testhic-foi15a1.rhcloud.com/api/countShplisteinz'
  }).then ( function successCallback (response){
    $scope.countShplisteinz = response.data
    console.log(response);
  },function errorCallback(response){
    console.log(response);
  });


  $scope.showAlertWrongCredetials = function() {
    var alertPopup = $ionicPopup.alert({
      title: 'Falsche Eingabe',
      template: 'Benutzername oder Passwort falsch!'
    });
  }
  $scope.showAlertFehler = function() {
    var alertPopup = $ionicPopup.alert({
      title: 'Fehler',
      template: 'Es ist ein Fehler aufgetreten! Bitte versuchen Sie es später erneut.'
    });
  }

})

.controller('loginCtrl', function($scope, $stateProvider, $urlRouterProvider, $authProvider) {

})

.controller('signupCtrl', function($scope) {

})
/* ##### Controller Einkaufslist #####
######################################*/
.controller('eListeCtrl', function($scope,$http,$window) {
  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/getProductShplist'
  }).then(function successCallback(response) {
    $scope.productsShplist = response.data
    console.log(response);
    if($scope.productsShplist==0){
      $scope.meldungShpl="Keine Produkte auf der Einkaufsliste ..."
    }
    else {
      $scope.meldungShpl="";
    }
  }, function errorCallback(response) {
    console.log(response);
  });

  $scope.doRefreshShplist=function(){
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/getProductShplist'
    }).then(function successCallback(response) {
      $scope.productsShplist = response.data
      if($scope.productsShplist==0){
        $scope.meldungShpl="Keine Produkte auf der Einkaufsliste ..."
      }
      else {
        $scope.meldungShpl="";
      }
      console.log(response);
    }).finally(function(){
      $scope.$broadcast('scroll.refreshComplete');
    });
  }

  $scope.countFunctionShp = function(id,operator){
    var elShp = angular.element( document.querySelector( '#proanzShp_'+id ) );
    var valueShp = parseInt(elShp.text(),10);
    if(operator == "+")
    valueShp = valueShp + 1
    else {
      var valShp = valueShp - 1
      if((valueShp - 1) >= 0)
      valueShp = valueShp - 1
    }
    elShp.html(valueShp)

    $scope.updateProduktAnzahlShp(valueShp,id)
    return valueShp
  }

  $scope.updateProduktAnzahlShp = function(anz,id){
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/setProAnzShp',
      data: {ProAnz:anz,productid:id}
    }).then(function successCallback(response){
      console.log(response);
      $scope.updateShopping_ListGet
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

  $scope.updateShopping_ListGet = function(){
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/getProductShplist'
    }).then(function successCallback(response) {
      $scope.productsShplist = response.data
      console.log(response);
    }, function errorCallback(response) {
      console.log(response);
    });
  }

  $scope.deleteProductFromShopping_List = function(product_id,shopping_list_id) {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/deleteProductShp',
      data: {'pro_id':product_id,'shop_list_id':shopping_list_id}
    }).then(function successCallback(response){
      $window.location.reload();
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

  deleteAllFromShopping_List = function(shopping_list_id) {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/deleteAllFromShopping_List',
      data: {'shop_list_id':shopping_list_id}
    }).then(function successCallback(response){
      $window.location.reload();
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

  $scope.itemsToInventory = function() {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/itemsToInventory'
      //data: {}
    }).then(function successCallback(response){
      $scope.antwort = response.data;
      deleteAllFromShopping_List($scope.antwort.shopping_list_id);
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

  $scope.singleItemToInventory = function(product_id,inventory_id) {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/singleItemToInventory',
      data: {'pro_id':product_id,'inv_id':inventory_id}
    }).then(function successCallback(response){
      $scope.deleteProductFromShopping_List(product_id,inventory_id);
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }


})
/* ##### Controller Inventar #####
######################################*/
.controller('inventoryCtrl', function($scope,$http,$window) {
  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/getProductInv'
  }).then(function successCallback(response) {
    $scope.productsInv = response.data
    if($scope.productsInv==0){
      $scope.meldungInv="Keine Produkte im Inventar ..."
    }
    else {
      $scope.meldungInv="";
    }
    console.log(response);
  }, function errorCallback(response) {
    console.log(response);
  });

  $scope.doRefreshInv=function(){
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/getProductInv'
    }).then(function successCallback(response) {
      $scope.productsInv = response.data
      if($scope.productsInv==0){
        $scope.meldungInv="Keine Produkte im Inventar ..."
      }
      else {
        $scope.meldungInv="";
      }
      console.log(response);
    }).finally(function(){
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.updateInventoryGet = function(){
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/getProductInv'
    }).then(function successCallback(response) {
      $scope.productsInv = response.data
      console.log(response);
    }, function errorCallback(response) {
      console.log(response);
    });
  }

  $scope.countFunction = function(id,operator){
    var el = angular.element( document.querySelector( '#proanz_'+id ) );
    var value = parseInt(el.text(),10);
    if(operator == "+")
    value = value + 1
    else {
      var val = value - 1
      if((value - 1) >= 0)
      value = value - 1
    }
    el.html(value)

    $scope.updateProduktAnzahl(value,id)
    return value
  }

  $scope.updateProduktAnzahl = function(anz,id){
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/setProAnzInv',
      data: {ProAnz:anz,productid:id}
    }).then(function successCallback(response){
      console.log(response);
      $scope.updateInventoryGet
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

  $scope.deleteProductFromInventory = function(product_id,inventory_id) {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/deleteProductInv',
      data: {'pro_id':product_id,'inv_id':inventory_id}
    }).then(function successCallback(response){
      $window.location.reload();
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

  $scope.transferToShoppingList = function(product_id,inventory_id) {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/transferToShoppingList',
      data: {'pro_id':product_id,'inv_id':inventory_id}
    }).then(function successCallback(response){
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }
})

/* ##### Controller Scanner#####
######################################*/
.controller('kameraCtrl', function($scope,$rootScope,$cordovaBarcodeScanner,$cordovaDialogs,$http,$location,$window,$ionicPopup) {

  $scope.scanBarcode = function() {
    $cordovaBarcodeScanner.scan().then(function(imageData) {
      $scope.searchEAN(imageData.text);
      console.log("Barcode Format -> " + imageData.format);
      console.log("Cancelled -> " + imageData.cancelled);
    }, function(error) {
      $cordovaDialogs.alert('error');
      console.log("An error happened -> " + error);
    });
  };

  $scope.searchEAN=function(ean_code){
    $rootScope.ean_code=ean_code;
    if(!!ean_code){
      $http({
        method:'POST',
        url: 'http://testhic-foi15a1.rhcloud.com/api/searchEAN',
        data: {EAN: ean_code}
      }).then(function successCallback(response){
        $scope.ergebnisEAN=response.data
        if ($scope.ergebnisEAN==0){
          $location.path();
          $location.path("/page1/page13")

        }
        else{
          $location.path();
          $location.path('/page1/page11')
        }
        if ($scope.ergebnis_id!==0){
          $rootScope.ergebnis_id=response.data[0].id
        }
        console.log(response);
      },function errorCallback(response){
        console.log(response);
      });
    }
    else {
      $ionicPopup.alert({
        title: 'Fehler',
        template: 'Kein Barcode erkannt!'
      });
    }
  };


  $scope.newProduct= {};
  $scope.newProductreset=function(){
    $scope.newProduct= {};
  }
  $scope.linkInv=function(){
    $location.path();
    $location.path("/page1/page9")
  }

  $scope.linkShplist=function(){
    $location.path();
    $location.path("/page1/page8")
  }
  $scope.window_reload=function(){
    $window.location.reload();
  }

  $scope.insertProductInvNew=function(){
    $http({
      method:'POST',
      url:'http://testhic-foi15a1.rhcloud.com/api/insertProductInvNew',
      data: {ProName: $scope.newProduct.ProName, EAN:$scope.ean_code, ProArt: $scope.newProduct.ProArt, ProAnz: $scope.newProduct.ProAnz}
    }).then(function successCallback(response){
      $scope.linkInv()
      $scope.newProductreset()
      console.log(response);
    }, function errorCallback(response){
      console.log(response);
    });
  }

  $scope.insertProductShplistNew=function(){
    $http({
      method:'POST',
      url:'http://testhic-foi15a1.rhcloud.com/api/insertProductShplistNew',
      data: {ProName:$scope.newProduct.ProName, EAN:$scope.ean_code, ProArt:$scope.newProduct.ProArt, ProAnz:$scope.newProduct.ProAnz}
    }).then(function successCallback(response){
      $scope.linkShplist()
      $scope.newProductreset()
      console.log(response);
    }, function errorCallback(response){
      console.log(response);
    });
  }

  $scope.insertProductInv=function(){
    $http({
      method:'POST',
      url:'http://testhic-foi15a1.rhcloud.com/api/insertProductInv',
      data: {id_product:$scope.ergebnis_id, ProAnz:$scope.newProduct.ProAnz}
    }).then(function successCallback(response){
      $scope.linkInv()
      console.log(response);
    }, function errorCallback(response){
      if(!!$scope.newProduct.ProAnz){
        $ionicPopup.alert({
          title: 'Fehler',
          template: 'Produkt bereits im Inventar!'
        });
      }
      else{
        $ionicPopup.alert({
          title: 'Fehler',
          template: 'Produktanzahl muss angegeben werden!'
        });
      }
      console.log(response);
    });
  }

  $scope.insertProductShplist=function(){
    $http({
      method:'POST',
      url:'http://testhic-foi15a1.rhcloud.com/api/insertProductShplist',
      data: {id_product:$scope.ergebnis_id, ProAnz:$scope.newProduct.ProAnz}
    }).then(function successCallback(response){
      $scope.linkShplist()
      console.log(response);
    }, function errorCallback(response){
      if(!!$scope.newProduct.ProAnz){
        $ionicPopup.alert({
          title: 'Fehler',
          template: 'Produkt bereits auf der Einkaufsliste!'
        });
      }
      else{
        $ionicPopup.alert({
          title: 'Fehler',
          template: 'Produktanzahl muss angegeben werden!'
        });
      }
      console.log(response);
    });
  }
})

.controller('scanErgebnisCtrl', function($scope) {

})

.controller('profileCtrl', function($scope,$http,$location,$ionicPopup) {
  $scope.newPassword={};
  $scope.changePasswort=function(passwort_real,passwort_confirm){
    if(passwort_real === passwort_confirm){

      $http({
        method: 'POST',
        url: 'http://testhic-foi15a1.rhcloud.com/api/changePassword',
        data: {password: passwort_real}
      }).then(function successCallback(response){
        $ionicPopup.alert({
          title: 'Erfolgreich',
          template: 'Passwort erforlgreich geändert.'
        });
        $location.path();
        $location.path("/page1/page12")
        console.log(response);
      }, function errorCallback(response){
        console.log(response);
      });
    }
    else {
      $ionicPopup.alert({
        title: 'Fehler',
        template: 'Passwörter stimmen nicht überein.'
      });
    }
  }

})

.controller('keinCodeInDBVorhandenCtrl', function($scope) {

})

.controller('productsiteCtrl', function($http,$scope,$stateParams,$state,$ionicHistory) {
  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/productsiteProduct?product_id='+$stateParams.product_id
  }).then(function successCallback(response){
    $scope.productname = response.data.ProName;
    $scope.proart = response.data.ProArt;
    $scope.proean = response.data.EAN;
  })
  errorCallback = function (error) {
    var alertPopup = $ionicPopup.alert({
    });
    alertPopup.then(function (res) {
    });
  }
  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/productsiteProductInv?product_id='+$stateParams.product_id
  }).then(function successCallback(response){
    $scope.invanz = response.data.ProAnz;
  })
  errorCallback = function (error) {
    var alertPopup = $ionicPopup.alert({
    });
    alertPopup.then(function (res) {
    });
  }
  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/productsiteProductShp?product_id='+$stateParams.product_id
  }).then(function successCallback(response){
    $scope.shpanz = response.data.ProAnz;
  })
  errorCallback = function (error) {
    var alertPopup = $ionicPopup.alert({
    });
    alertPopup.then(function (res) {
    });
  }

  $scope.doRefreshProductsite = function() {
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/productsiteProduct?product_id='+$stateParams.product_id
    }).then(function successCallback(response){
      $scope.productname = response.data.ProName;
      $scope.proart = response.data.ProArt;
      $scope.proean = response.data.EAN;
    })
    errorCallback = function (error) {
      var alertPopup = $ionicPopup.alert({
      });
      alertPopup.then(function (res) {
      });
    }
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/productsiteProductInv?product_id='+$stateParams.product_id
    }).then(function successCallback(response){
      $scope.invanz = response.data.ProAnz;
    })
    errorCallback = function (error) {
      var alertPopup = $ionicPopup.alert({
      });
      alertPopup.then(function (res) {
      });
    }
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/productsiteProductShp?product_id='+$stateParams.product_id
    }).then(function successCallback(response){
      $scope.shpanz = response.data.ProAnz;
    })
    errorCallback = function (error) {
      var alertPopup = $ionicPopup.alert({
      });
      alertPopup.then(function (res) {
      });
    }
  }

  $scope.myGoBack = function() {
    $state.go('tabsController.inventory');
    //$ionicHistory.goBack();
  };
})
