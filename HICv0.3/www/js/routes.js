angular.module('app.routes', ['satellizer', 'ui.router'])

.config(function($stateProvider, $urlRouterProvider, $authProvider, $provide, $ionicConfigProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $authProvider.loginUrl = 'http://testhic-foi15a1.rhcloud.com/api/authenticate';

    $urlRouterProvider.otherwise('/login');

    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.navBar.alignTitle('center')


  $stateProvider
      .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('start', {
    url: '/page5',
    templateUrl: 'templates/start.html',
    controller: 'startCtrl'
  })

  .state('login', {
    url: '/page6',
    templateUrl: 'templates/login.html',
    controller: 'AuthCtrl'
  })

  .state('signup', {
    url: '/page7',
    templateUrl: 'templates/signup.html',
    controller: 'AuthCtrl'
  })

  .state('changePasswort', {
    url: '/page14',
    templateUrl: 'templates/changePassword.html',
    controller: 'profileCtrl'
  })

  .state('tabsController.eListe', {
    url: '/page8',
    views: {
      'tab1': {
        templateUrl: 'templates/eListe.html',
        controller: 'eListeCtrl'
      }
    }
  })

  .state('tabsController.inventory', {
    url: '/page9',
    views: {
      'tab2': {
        templateUrl: 'templates/inventory.html',
        controller: 'inventoryCtrl'
      }
    }
  })

  .state('tabsController.kamera', {
    url: '/page10',
    views: {
      'tab4': {
        templateUrl: 'templates/kamera.html',
        controller: 'kameraCtrl'
      }
    }
  })

  .state('tabsController.scanErgebnis', {
    url: '/page11',
    views: {
      'tab4': {
        templateUrl: 'templates/scanErgebnis.html',
        /*controller: 'scanErgebnisCtrl'*/
        controller: 'kameraCtrl'
      }
    }
  })

  .state('tabsController.profile', {
    url: '/page12',
    views: {
      'tab3': {
        templateUrl: 'templates/profile.html',
        controller: 'AuthCtrl'
      }
    }
  })

  .state('tabsController.keinCodeInDBVorhanden', {
    url: '/page13',
    views: {
      'tab4': {
        templateUrl: 'templates/keinCodeInDBVorhanden.html',
        controller: 'kameraCtrl'
      }
    }
  })

  .state('productsite', {
    url: '/page9/:product_id',
    templateUrl: 'templates/productsite.html',
    controller: 'productsiteCtrl'
  });

  function redirectWhenLoggedOut($q, $injector) {
      return {
          responseError: function (rejection) {
              var $state = $injector.get('$state');
              var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

              angular.forEach(rejectionReasons, function (value, key) {
                  if (rejection.data.error === value) {
                      localStorage.removeItem('user');
                      $state.go('login');
                  }
              });

              return $q.reject(rejection);
          }
      }
  }
$provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

$urlRouterProvider.otherwise('/page5')



});
